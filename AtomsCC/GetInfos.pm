package AtomsCC::GetInfos;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use LWP::Simple;
use LWP::UserAgent;
use Digest::file qw(digest_file_hex);
use Archive::Extract;
use File::Basename;
use File::Find;
use File::Copy;
use File::Path;

use AtomsCC::Log;
use AtomsCC::SysUtils;

#get_ini();

our @ISA = ('Exporter');
our @EXPORT = qw(&base_toolbox_url &stage_parseDESCRIPTION &stage_Download &stage_downloadDESCRIPTION &stage_SetBinaryName &stage_Unpack);

sub base_toolbox_url()
{
    my $url = $::atoms_server.'toolboxes/'.$::toolbox_name.'/';
    if(defined($::toolbox_version)) {
        $url .= $::toolbox_version.'/';
    }
    return $url;
}

sub stage_downloadDESCRIPTION
{
    my $description_file = $::work.$::sep.'downloads'.$::sep.'DESCRIPTION';

    my $file_url = base_toolbox_url() . 'DESCRIPTION';

    verbose("Downloading $file_url to ".$description_file);
    my $http_code = getstore($file_url,$description_file);
    if ($http_code  == 404 )
    {
        warning("DESCRIPTION not found at URL ".$file_url.". Are you sure that the toolbox '".$::toolbox_name."' exists".(defined($::toolbox_version) ? " in version ".$::toolbox_version : "")." ?");
        return;
    }

    if ($http_code  == 500 )
    {
        error("Internal server error while getting DESCRIPTION at $file_url.");
    }

    return $description_file;
}

sub stage_Download
{
    if( ! ( defined($::source{'name'}) && defined($::source{'url'})))
    {
        error('This toolbox doesn\'t contain a valid sources archive. Maybe the maintainer has not uploaded the sources ?');
    }

    my $download = $::work.$::sep.'downloads'.$::sep.$::source{'name'};

    info("Downloading source archive from ".$::source{'url'});
    if( getstore($::source{'url'},$download) != 200 )
    {
        error("Failed to download source file from URL ".$::source{'url'});
    }

    if( digest_file_hex($download,"MD5") ne $::source{'md5'} )
    {
        error("MD5SUMS mismatch for source archive downloaded from URL ".$::source{'url'}.": ".
              "Downloaded MD5: ".digest_file_hex($download,"MD5").", given by server: ".$::source{'md5'});
    }

    verbose('Sources downloaded from '.$::source{'url'}.' to '.$download);
    return $download;
}


sub debug
{
    # Uncomment next line to debug DESCRIPTION parsing
    #verbose($_[0]);
}

sub stage_parseDESCRIPTION
{
    my $description_file = $_[0];
    if ( ! -e $description_file ) {
        error("DESCRIPTION file not found in sources: cannot build");
    }

    my $count = 0;
    my $previous_key = '';

    open(FILEHANDLER, $description_file);
    while(<FILEHANDLER>)
    {
        my $var = $_;
        my $key = '';

        # remove windows as well as unix end-of-lines
        $var =~ s/[\x0A\x0D\n]//g;

        if($var =~ m/^\/\//) {
            debug("comment: ".$var);
            next;
        }

        if($var =~ m/^(\w+): /){
            $key = $1;
            $var =~ s/^${key}: //;
            $previous_key = '';
            debug("found new key: ".$key);
        }

        if (($var eq '') && (! (($key eq 'Description') || ($previous_key eq 'Description')))) {
            next;
        }

        # single-line fields
        if (grep( /^${key}$/, ('Toolbox', 'Title', 'Summary',
                               'Version',
                               'Date', 'ScilabVersion', 'License',
                               'URL', 'Entity',
                               'WebSite', # should be Website (lowercase 's')... oh well...
                               'HasNativeCode'))) {
            if ($key eq 'Toolbox') {
                $key = 'name';
            }
            $::toolbox{lc($key)} = $var;
            debug(" - NEW $key: ".$var);
            next;
        }

        if (grep( /^${key}$/, ('sourceName', 'sourceMd5', 'sourceSha1',
                               'sourceSize', 'sourceUrl', 'sourceFileClass',
                               'sourceSupportedScilab'))) {
            $key =~ s/^source//;
            $::source{lc($key)} = $var;
            debug(" - SRC $key: ".$var);
            next;
        }

        # multi-line fields
        if (grep( /^${key}$/, ('Author', 'Maintainer', 'Category', 'Depends', 'Description'))) {
            if ($key eq 'Description') {
                $::toolbox{lc($key)} = $var."\n";
                debug(" - NEW ${key}: ".$var);
            }
            else {
                $::toolbox{lc($key).'_0'} = $var;
                debug(" - NEW ${key}_0: ".$var);
            }
            $count = 1;
            $previous_key = $key;
            next;
        }

        # field continuation
        if ( (! ($previous_key eq '') ) && ($var =~ m/^ (.*)/) ) {
            $var = $1;

            if ($previous_key eq 'Description') {
                $::toolbox{lc($previous_key)} .= $var."\n";
                debug(" - ADD ${previous_key}: ".$var);
            }
            else {
                $::toolbox{lc($previous_key).'_'.$count} = $var;
                debug(" - ADD ${previous_key}_$count: ".$var);
            }
            $count = $count+1;
            next;
        }

        if ( ! ($var eq '') ) {
            warning('Unparsable line found in DESCRIPTION, skipped: "'.$_.'"');
        }
    }

    close(FILEHANDLER);
}

sub stage_SetBinaryName
{
    my $extension;
    if( ! $::HAS_NATIVE_CODE ) {
        $extension = '.bin.zip';
    }
    elsif( $::WINDOWS ) {
        if ($::ARCH eq '32') {
            $extension = '.bin.windows.zip';
        }
        else { # ($::ARCH eq '64')
            $extension = '.bin.x64.windows.zip';
        }
    }
    elsif( $::LINUX ) {
        if ($::ARCH eq '32') {
            $extension = '.bin.i686.linux.tar.gz';
        }
        else { # ($::ARCH eq '64')
            $extension = '.bin.x86_64.linux.tar.gz';
        }
    }
    elsif( $::MACOSX ) {
        if ($::ARCH eq '32') {
            $extension = '.bin.i686.darwin.tar.gz';
        }
        else { # ($::ARCH eq '64')
            $extension = '.bin.x86_64.darwin.tar.gz';
        }
    }

    my $binary_file = $::toolbox{'name'}.'_'.$::toolbox{'version'}.'_'.$::scilab_version_short.$extension;
    $::binary = $::work.$::sep.'binaries'.$::sep.$binary_file;
    verbose('Binary will be generated at '.$::binary);
}

sub stage_Unpack
{
    my $download = $_[0];
    my $sources;

    if (index(lc(basename($download)), lc($::toolbox_name)) != -1) {
        $sources = $::work.$::sep.'sources'.$::sep.basename($download);
    }
    else {
        $sources = $::work.$::sep.'sources'.$::sep.$::toolbox_name;
    }

    mkdir(dirname($sources));

    if ( -d $download ) {
        copyDir($download, $sources);
    }
    else {
        my $ae = Archive::Extract->new( archive => $download );

        verbose("Unpacking the archive $download to source folder ".$sources);
        unless( $ae->extract( to => $sources ) ) {
            error("Extract of $download has failed:".$ae->error);
        }
    }

    verbose("Searching 'macros' folder in $sources...");

    # we'll suppose the root is the shortest folder containing a 'macros' folder
    my $macrodir = '';
    find( sub { if (($_ eq 'macros') && ( -d $File::Find::name) && length($File::Find::name) > length($macrodir)) { $macrodir = $File::Find::name; } },
          $sources);
    if ($macrodir eq '') {
        error("No 'macros' folder found in sources. This toolbox does not follow the structure of the Toolbox skeleton, and cannot be built");
    }

    return dirname($macrodir);
}

1;
