package AtomsCC::Publish;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#
use strict;
use Exporter;

require LWP::UserAgent;
require HTTP::Cookies;

use HTTP::Request::Common;
use LWP::Simple qw(getstore);
require LWP::UserAgent;
use CGI qw(:standard);

require Config::IniFiles;
use File::Basename;
use Digest::file qw(digest_file_hex);

use AtomsCC::Log;
use AtomsCC::SysUtils;
use AtomsCC::GetInfos;

our @ISA = ('Exporter');
our @EXPORT = qw( &load_ini_file &login &stage_Publish &do_publish );

sub ini_file_location
{
    return $::root.$::sep.'ini'.$::sep.'AtomsCC.ini';
}

sub load_ini_file
{
    my $initfilename = ini_file_location();
    if ( ! -e $initfilename ) {
        $::atoms_server = 'http://atoms.scilab.org/';
        return;
    }

    my $inifile = Config::IniFiles->new( -file => $initfilename);

    if (! $inifile->exists('Atoms','server')) {
        error("Init file at $initfilename must contain [Atoms] server= setting");
    }
    $::atoms_server = $inifile->val('Atoms','server');
    if (! (substr($::atoms_server, -1) eq '/')) {
        $::atoms_server .= '/'; # must have a final '/'
    }

    if ( ! defined($::atoms_login) ) {
        $::atoms_login = $inifile->val('Atoms','login');
    }

    if ( ! defined($::atoms_password) ) {
        $::atoms_password  = $inifile->val('Atoms','password');
    }
}

sub login
{
    if (((length( $::atoms_login || '')) == 0) || 
        ((length( $::atoms_password || '')) == 0) )
    {
        if ( -e ini_file_location() )
        {
            error("Atoms login/password not defined on the command line, nor in the '".ini_file_location()."' config file: cannot publish");
        }
        else {
            error("Atoms login/password not defined on the command line, and no config file at '".ini_file_location()."': cannot publish");
        }
    }

    verbose("We will publish; checking that we can login to ".$::atoms_server." as ".$::atoms_login."...");

    # keep the browser as static to upload after login
    $::ua = LWP::UserAgent->new( );
    $::cookie_jar = HTTP::Cookies->new( );
    $::ua->cookie_jar( $::cookie_jar );

    my $response = $::ua->request( GET $::atoms_server."login");
    $::cookie_jar->extract_cookies( $response );

    $response = $::ua->request( POST $::atoms_server."login",
                              [ 'action' => 'login',
                                'form_email' => $::atoms_login,
                                'form_password' => $::atoms_password,
                                'remember_me' => 'forever' ] );
    $::cookie_jar->extract_cookies( $response );

    my $output = $response->as_string();
    $output =~ s/\n//g;
    if ( $output =~ m/\<div\ class\=[\"\']msg_error[\"\']\>([^<]*)<\/div\>/ ) {
        error("Failed to sign in to ".$::atoms_server." with login ".$::atoms_login.": ".$1);
    }

    verbose("... Successfully logged in to ".$::atoms_server." as ".$::atoms_login.".");
}

sub stage_Publish
{
    verbose('Publishing toolbox to the Atoms server...');

    if (! -e $::build_log) {
        error("Cannot publish: build has not run yet (build log not found at ".$::build_log.")");
    }
    if (! -e $::test_log) {
        error("Cannot publish: tests have not run yet (test log not found at ".$::test_log.")");
    }

    my $binary_description;
    my $binary_file_class;
    my $binary_type = 'application/x-gzip'; # only different on Windows, see below

    if( ! $::HAS_NATIVE_CODE ) {
        $binary_description = 'Binary version (all platforms)';
        # $binary_file_class is not necessary
    }
    elsif($::WINDOWS ) {
        if ($::ARCH eq '32') {
            $binary_description = 'Windows version (i686)';
            $binary_file_class = 'windows binary 32';
        }
        else { #  ($::ARCH eq '64')
            $binary_description = 'Windows version (x64)';
            $binary_file_class = 'windows binary 64';
        }
        $binary_type = 'application/zip';
    }
    elsif($::LINUX ) {
        if ($::ARCH eq '32') {
            $binary_description = 'Linux version (i686)';
            $binary_file_class = 'linux binary 32';
        }
        else { #  ($::ARCH eq '64')
            $binary_description = 'Linux version (x86_64)';
            $binary_file_class = 'linux binary 64';
        }
    }
    elsif( $::MACOSX ) {
        $binary_description = 'MacOSX version';
        $binary_file_class = 'macosx binary 64';
    }

    do_publish($binary_description, $binary_file_class, $binary_type);
}

sub do_publish
{

    my $binary_description = shift;
    my $binary_file_class = shift;     
    my $binary_type = shift;
    
    $binary_description .= "\nAutomatically generated by the ATOMS compilation chain\n";

    my $file_name;
    my $download_url;

    # check if file already exists
    my $res;
    my $increment = 0;
    do
    {
        $file_name = basename($::binary);
        if ($increment > 0) {
            $file_name =~ s/\.bin\./-${increment}.bin./g ;
        }
        $download_url = base_toolbox_url() . 'files/'.$file_name;
        verbose("Checking if a binary already exists at URL $download_url (iteration: $increment)...");
        $res = $::ua->head($download_url);
        verbose("... check result is (1 = found): ".$res->is_success. " and HTTP code: ".$res->code);
        $increment += 1;        

        # the error codes correspond to the errors sent by download_file.php in atoms-website.git
        # do not make modifications here without checking there
        if ($res->code == HTTP::Status::HTTP_INTERNAL_SERVER_ERROR) {
            warning("Server returned an error 500 when checking if a binary already exists at URL $download_url: skipped, but maybe the server is missing some files");
        }
    }
    # stop if not forced, or if not found, or if overflow
    while ( $::FORCE_PUBLISH && $res->code != HTTP::Status::HTTP_NOT_FOUND && ($increment < 20) );

    if ($increment eq 20) {
        error("failed to generate unique name for binary after trying 20 times");
    }

    if ( $res->code != HTTP::Status::HTTP_NOT_FOUND) {
        error("File $file_name already exists on the server. Please re-run with --force if you really want to replace it.");
    }

    my $page_url = $::atoms_server."admin/".$::toolbox_name."/".$::toolbox_version."/files";
    my $response = $::ua->request( GET $page_url );
    my $output = $response->as_string();
    if ( ! ($output =~ m/form name=\"file_add\"/ ) ) {
        error("Failed to access page $page_url. Are you sure you are a maintainer of this package?");
    }

    verbose("Uploading binary file...");
    my %content = ( "action" => "add_file",
                    "description" => $binary_description,
                    "enable" => 1,
                    "form_scilab_supported_version" => $::scilab_version_short,
                    "upload" => [ $::binary, $file_name,
                                  Content_Type => $binary_type ]
                    );

    if ( $::HAS_NATIVE_CODE ) {
        $content{"form_file_class"} = $binary_file_class;
    }

    verbose("  Posting to $page_url");
    use Data::Dumper;
    verbose("  POST data is: ".Dumper(\%content));

    $response = $::ua->request( POST $page_url,
                                Content_Type => 'form-data',
                                Content      => \%content);
    verbose("...Uploaded, now verifying if server has it...");    
    $output = $response->as_string();
    unless( open(UPLOAD_LOG,'> '.$::upload_log) ) {
        error('Failed to create the "'.$::upload_log.'" file. Maybe the folder is not writeable?');
    }
    print UPLOAD_LOG $output;
    close UPLOAD_LOG;

    if ( ! ($output =~ m/${file_name}/ ) ) {
        # The first page sometimes doesn't have the new file (something must be happenin in the background).
        # Wait a little and reload the page..
        verbose("String '$file_name' not found in content of posted page '$page_url', maybe upload failed, waiting a little and retrying to get page...");
        sleep(1);
        my $response = $::ua->request( GET $page_url );
        my $output = $response->as_string();
        if ( ! ($output =~ m/${file_name}/ ) ) {
            verbose("String '$file_name' not found in content of page '$page_url', maybe upload failed, stopping");
            error("Upload of file $file_name to $page_url seems to have failed (file does not appear on page). Check the upload output at ".$::upload_log);
        }
    }

    my $released_binary = $::work.$::sep.'downloads'.$::sep."released-".$file_name;
    if( getstore($download_url, $released_binary) != 200 ) {
        error("Could not re-download published file from $download_url. Maybe it failed to upload? Check the upload output at ".$::upload_log);
    }

    my $binary_md5 = digest_file_hex($::binary,"MD5");
    my $released_binary_md5 = digest_file_hex($released_binary,"MD5");
    if (! ( $binary_md5 eq $released_binary_md5 )) {
        error("Local release $file_name and file uploaded to server at $download_url have different MD5 sum. Please remove the file from the server and try again...");
    }
    verbose("MD5 between release $file_name and uploaded file on $download_url is OK");
    verbose('Binary file successfully uploaded');
    info("Toolbox successfully published to ".$page_url." as ".$file_name);
    return 0;
}

1;
