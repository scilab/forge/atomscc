package AtomsCC::Log;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;

our @ISA         = qw(Exporter);
our @EXPORT = qw( &verbose &info &warning &error &had_warnings );


sub verbose
{
    if ($::VERBOSE) {
        print($_[0]."\n");
    }
}

sub info
{
    print($_[0]."\n");
}

sub warning
{
    print("WARNING: ".$_[0]."\n");
    $::had_warnings = 'yes';
}

sub error
{
    print("ERROR: ".$_[0]."\n");
    exit(1);
}

sub had_warnings
{
    return (defined($::had_warnings));
}

1;
