package AtomsCC::Pack;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use File::Path;
use File::Basename;
use File::Copy;
use LWP::Simple;
use Digest::file qw(digest_file_hex);
use Cwd;

use AtomsCC::Log;

our @ISA = ('Exporter');
our @EXPORT = qw( &stage_Pack  );

sub stage_Pack
{
    verbose('Packaging the toolbox binary');

    my $compilation_path = $_[0];

    # Delete the precedent binaries if necessary
    if(-e $::binary) {
        unless(unlink($::binary)) {
            error('Failed to delete previous binary file '.$::binary);
        }
    }

    # Go to the "compilation" directory
    unless( chdir($::work.$::sep.'compil'.$::sep) ) {
        error('The directory "'.$::work.$::sep.'compil'.$::sep.'" isn\'t accessible');
    }
    
    # Create the FILES file that contain the file list and its MD5SUMs    
    my $FILES = $compilation_path.$::sep.'FILES';
    verbose("Creating FILES files $FILES");
    unless( open(FILES,'> '.$FILES) ) {
        error('Failed to create the "'.$FILES.'" file. Maybe the folder is not writeable?');
    }
    
    directory_to_FILES($compilation_path );
    close(FILES);
    
    my $pack_cmd;
    if( $::WINDOWS )
    {
        my $zip_path = $::root.$::sep.'tools'.$::sep.'zip.exe';
        if ( ! -e $zip_path ) {
            error("Could not find zip executable at $zip_path");
        }
        $pack_cmd = '"'.$zip_path.'" '.$::binary.' -r '.basename($compilation_path).' > '.$::pack_log.' 2>&1';
    }
    else # ( $::LINUX || $::MACOSX )
    {
    	if( ! $::HAS_NATIVE_CODE ){
    	    $pack_cmd = '(zip -9 -r '.$::binary." ".basename($compilation_path).') > '.$::pack_log.' 2>&1';
    	}
    	else{
            $pack_cmd = '(tar cf - '.basename($compilation_path).' | gzip -9 >  '.$::binary.') > '.$::pack_log.' 2>&1';
    	}
    }
    
    my $return_code = system($pack_cmd);
    if( $return_code != 0 )
    {
        error('Packaging command failed. For details see log file at '.$::pack_log);
    }
    
    info('Toolbox binary successfully created as '.$::binary);
}

# ==============================================================================
# directory_to_FILES
#
# Create the FILES file that contain the list of all files of the binary version
# with its MD5SUMS
# ==============================================================================

sub directory_to_FILES
{
    my $dir = $_[0];
    my @list_dir;
    my $current_directory;
    
    # On enregistre le répertoire dans lequel on se situe � l'entr�e de la fonction
    my $previous_directory = getcwd();
    
    chdir($dir);
    
    @list_dir = <*>;
    
    foreach my $list_dir (@list_dir)
    {
        $current_directory = getcwd();
        
        if(-d $list_dir)
        {
            directory_to_FILES($current_directory.$::sep.$list_dir);
        }
        
        if( (-f $list_dir ) && ! ($list_dir =~ m/~$/ ) && ($list_dir ne 'FILES') )
        {
            my $str = '';
            $str .= digest_file_hex($list_dir,"MD5");
            $str .= ' ';
            $str .= substr( $current_directory.$::sep.$list_dir , length($dir.$::sep) );
            print FILES $str."\n";
        }
    }
    
    chdir($previous_directory);
}

1;
