package AtomsCC::Check;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#
use strict;
use Exporter;
use File::Path;
use File::Basename;
use File::Find;

use AtomsCC::Log;

our @ISA = ('Exporter');
our @EXPORT = qw( &stage_Check );

sub stage_Check
{
    verbose("Checking that source file has all the needed files...");

    my $compilation_path = $_[0];

    my %required_files = ();
    $required_files{'DESCRIPTION'}                       = 'mandatory';
    $required_files{'builder.sce'}                       = 'mandatory';
    $required_files{'loader.sce'}                        = 'warning';
    $required_files{'etc'.$::sep.$::toolbox{'name'}.'.start'}  = 'optional';
    $required_files{'etc'.$::sep.$::toolbox{'name'}.'.quit'}   = 'optional';

    # Required Files
    foreach my $file (keys %required_files)
    {
        if (-e $compilation_path.$::sep.$file) {
            # all good
            next;
        }
        if( $required_files{$file} eq 'mandatory' )
        {
            if($file eq 'etc'.$::sep.$::toolbox_name.'.start' || $file eq 'etc'.$::sep.$::toolbox_name.'.quit'){
                chdir($compilation_path.$::sep.'etc');
                my @list = <*>;
                foreach my $element (@list){
                    if (($element =~ '.start') ||
                        ($element =~ '.quit') ||
                        (lc($element) eq lc($::toolbox_name.'.start')) ||
                        (lc($element) eq lc($::toolbox_name.'.quit'))) {
                        warning("The file ".$compilation_path.$::sep.'etc'.$::sep.$element." doesn\'t match the toolbox name.");
                    }
                }
#                if(! -e $compilation_path.$::sep.'etc'.$::sep.$::toolbox_name.'.quit'){
#                    open(FILEHANDLER,'>'.$compilation_path.$::sep.'etc'.$::sep.$::toolbox_name.'.quit');
 #                   close(FILEHANDLER);
  #              }
            }

            if(! -e $compilation_path.$::sep.$file){
                error('The file "'.$file.'" isn\'t present in the source tree.');
            }
        }
        elsif( $required_files{$file} eq 'optional' ) {
            warning('The file "'.$file.'" isn\'t present in the source tree');
        }
    }

    # hash of all relative paths
    my %files = ();
    find( sub {
	if (! ($File::Find::name eq $compilation_path )) {
	    $files{substr($File::Find::name, length($compilation_path)+1)} = 1;
	}
	  }, ( $compilation_path ));


    foreach (grep { $_ =~ m#^macros/# } keys %files)
    {
        if(! /(\.sc[ie]|\/)$/ ) {
	    # TODO: this should be in tbx_builder_macros(), not here
            warning('macros directory must contain only .sci and .sce files');
            last;
        }
    }

    foreach (grep { $_ =~ /\.f$/} keys %files)
    {
        if(!m#^src/fortran/# && !m#^sci_gateway/fortran/#)
        {
            warning('"'.$_.'" is a fortran source and should be in "src'.$::sep.'fortran"');
        }
    }

    foreach (grep { $_ =~ /\.[ch]$/} keys %files)
    {
        if(!m#^(src/(c|cpp)|sci_gateway/(c|fortran|cpp))/|includes/#)
        {
            warning('"'.$_.'" is a C source and should be in "src'.$::sep.'c, sci_gateway'.$::sep.'c or sci_gateway'.$::sep.'fortran');
        }
    }

    # Constraints: if $key exists, $constraints{$key} must exist
    my %constraints = (
        qr#^help/([a-z][a-z]_[A-Z][A-Z])/[^/]+\.xml$# => sub{ "help/$1/build_help.sce" },
        qr#^help/([a-z][a-z]_[A-Z][A-Z])/build_help.sce$# => sub{ "help/builder_help.sce" },
        qr#^macros/.+\.sc[ie]$# => sub{ "macros/buildmacros.sce" });

    # Build constraints for allowed languages
    my %languages = (
        "c" => qr/[ch]/,
        "fortran" => qr/f/);

    foreach (keys %languages)
    {
        # if src/(lang) has source files, src/(lang)/builder_(lang).sce must exist
        $constraints{qr#^src/($_)/.+\.$languages{$_}$#} = sub{ "src/$1/builder_$1.sce" };

        # if sci_gateway/(lang) has C sources, sci_gateway/(lang)/builder_gateway_(lang).sce
        # must exist
        $constraints{qr#^sci_gateway/($_)/.+[ch]$#} = sub{ "sci_gateway/$1/builder_gateway_$1.sce" };

        # if src/(lang)/builder_(lang).sce exist, src/builder_src.sce must exist
        $constraints{qr#^src/$_/builder_$_.sce$#} = sub{ "src/builder_src.sce" };

        # if sci_gateway/(lang)/builder_gateway_(lang).sce exist, sci_gateway/builder_gateway.sce must exist
        $constraints{qr#^sci_gateway/$_/builder_gateway_$_.sce$#} = sub{ "sci_gateway/builder_gateway.sce" };
    }

    # Check constraints
    foreach my $constraint (keys %constraints)
    {
        foreach my $file (keys %files)
        {
            if($file =~ $constraint)
            {
                my $required = $constraints{$constraint}();

                unless(defined($files{$required}))
                {
                    warning('"'.$&.'" needs "'.$required.'", which isn\'t in the archive');
                }
            }
        }
    }
}

1;
