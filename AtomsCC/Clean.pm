package AtomsCC::Clean;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use File::Path;
use File::Basename;
use File::Copy;
use Cwd;
use Config;

use AtomsCC::Log;
use AtomsCC::SysUtils;


our @ISA = ('Exporter');
our @EXPORT = qw( &basic_initialization &stage_Clean &stage_CleanScilab );


sub basic_initialization
{
    $::WINDOWS = ($^O eq 'MSWin32')?1:0;
    $::LINUX   = ($^O eq 'linux')?1:0;
    $::MACOSX  = ($^O eq 'darwin')?1:0;
    $::ARCH    = ($Config{longsize}  == 8 || $Config{archname} =~ "x64")?'64':'32';
    
    if ( ! ( $::WINDOWS || $::LINUX || $::MACOSX) ) {
        error("Neither Mac, Windows nor Linux: platform not supported, or there is a bug");
    }

    if ($::WINDOW) {
        $::DEFAULT_SCILAB_PATH='C:\\Program Files';
    }
    if ($::LINUX) {
        $::DEFAULT_SCILAB_PATH=$ENV{"HOME"};
    }
    if ($::MACOSX) {
        $::DEFAULT_SCILAB_PATH='/Applications';
    }
    
    # $::root may not be the same as cwd, if we run from a different folder
    $::root = Cwd::abs_path(File::Spec->rel2abs(dirname($0)));
    if ( ! File::Spec->file_name_is_absolute( $::root) ) {
        # be paranoid :)
        error("Could not get absolute path for the root $::root. This is a bug in AtomsCC.");
    }
    
    $::sep = "/"; # basically useless, could be removed, just I don't have time

    my $cwd = File::Spec->rel2abs(getcwd());
    $::work = $cwd.$::sep.'work';
    verbose("Work directory is: ".$::work);

    $::test_log = $::work.$::sep.'logs'.$::sep.'test.log';
    $::build_log = $::work.$::sep.'logs'.$::sep.'build.log';
    $::pack_log = $::work.$::sep.'logs'.$::sep.'pack.log';
    $::upload_log = $::work.$::sep.'logs'.$::sep.'upload.html';
}

sub stage_Clean
{
    # do not delete if the sources are in work
    # (could be improved: use a unique build folder, etc; but Jenkins should take care of that)
    if(defined($::LOCALSOURCES)) {
        if (index($::LOCALSOURCES, $::work) == 0) {
            error("Cannot build from sources that are in the \"work\" folders: they would be erased when building");
        }
    }

    if ( -e $::work ) {
        deleteDir($::work, 1);
    }

    if ( -e $::work ) {
        error("Failed to delete previous work directory $::work. Maybe another process is accessing it ?")
    }
    unless ( mkdir($::work) &&
             mkdir($::work.$::sep.'downloads') &&
             mkdir($::work.$::sep.'sources') &&
             mkdir($::work.$::sep.'compil') &&
             mkdir($::work.$::sep.'scripts') &&
             mkdir($::work.$::sep.'binaries') &&
             mkdir($::work.$::sep.'logs') ) {
        error("Failed to create work directory $::work and its subfolders");
    }
}

sub stage_CleanScilab
{
    # Cleaning atoms files and folder from:
    # <scilab>/share/scilab/.atoms
    # <scilab>/share/scilab/contrib (except files already installed, and 'archive' folder)
    # <scilab>contrib/archives/<toolbox.zip>
    # <home>.Scilab/<version>
    
    verbose('Clearing Scilab installation from files created:');
    my $warns_if_not_found = $_[0];
    my $path_to_archive;
    my $path_to_pref;

    if($::WINDOWS ) {
         # TODO: test on Windows
    	$path_to_archive = dirname(dirname(find_scilab()));
        $path_to_pref = $ENV{"USERPROFILE"}."\\AppData\\Roaming\\Scilab\\scilab-".$::scilab_version;
    }
    else { # ( $::LINUX || $::MACOSX )
         # TODO: test on Linux
    	$path_to_archive = dirname(dirname(find_scilab())).'/share/scilab';
	$path_to_pref = $ENV{"HOME"}.'/.Scilab/scilab-'.$::scilab_version;
    }
    
    if (-d $path_to_archive.$::sep.".atoms"){
        deleteDir($path_to_archive.$::sep.".atoms",1);
        verbose('Deleting Atoms cache directory '.$path_to_archive.$::sep.".atoms");
    }
    else {
        if ($warns_if_not_found) {
            warning('Atoms cache directory '.$path_to_archive.$::sep.".atoms".' not found, maybe something is wrong in cleanup');
        }
    }
    
    # delete everything from contrib except preinstalled files
    if (-d $path_to_archive.$::sep."contrib"){
        chdir($path_to_archive.$::sep."contrib");
        verbose('Clearing directory '.$path_to_archive.$::sep."contrib, (except files already installed, and 'archive' folder)");
        my @files = <*>;
        foreach my $file (@files)
        {
            if ( $file =~ /^Makefile/ || 
                   $file =~ /toolbox_skeleton/ || # toolbox_skeleton and xcos_toolbox_skeleton
                   $file eq 'loader.sce' || 
                   $file eq "archives" ) {
                next;
            }

            if(-d $path_to_archive.$::sep."contrib".$::sep.$file){
                deleteDir($path_to_archive.$::sep.'contrib'.$::sep.$file,1);
            }
            elsif(-e $path_to_archive.$::sep."contrib".$file)
            {
                unlink($path_to_archive.$::sep.'contrib'.$::sep.$file);
            }
    	}
    }
    else {
        warning('Contrib directory '.$path_to_archive.$::sep."contrib".' not found, maybe something is wrong in cleanup');
    }

    if (-e $path_to_archive.$::sep."contrib".$::sep."archives".$::sep.basename($::binary)){
        unlink($path_to_archive.$::sep."contrib".$::sep."archives".$::sep.basename($::binary));
        verbose('Deleting file '.$path_to_archive.$::sep."contrib".$::sep."archives".$::sep.basename($::binary));
    }

    if (-d $path_to_pref)
    {
        deleteDir($path_to_pref,1);
        verbose('Deleting preference directory '.$path_to_pref);
    }
    else {
        if ($warns_if_not_found) {
            warning('Preference directory '.$path_to_pref.' not found, cannot delete');
        }
    }

    return 0;
}

1;
